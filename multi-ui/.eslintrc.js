module.exports = {
  root: true,
  extends: ['@xueyi'],
  globals: {
    Fn: true,
    ElRef: true,
    PropType: true,
    Nullable: true,
    Recordable: true,
  },
};
